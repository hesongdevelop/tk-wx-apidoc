#TK客服微信子系统API文档

##概述

该项目是一个API手册。它定义了TK客服微信子系统API，主要包括其各个组成部分的RPC接口。

文档使用 [Sphinx Documentation](http://sphinx-doc.org/) 编写。

##编译

大致过程：

* 安装 Python

* 安装支持 pypi 的 Python 包管理器，如 setuptools + pip

* 安装该项目的依赖包，如 Shpinx 等

以 pip 包管理器为例：执行

```sh
cd <该文档所在目录>
pip install -r requirements.txt
```

* 安装中文字体

本项目使用了 seqdiag 等产生绘图，它需要中文字体的支持。

首先，找到要使用的中文矢量字体的路径，如 C:\Windows\font\simsun.ttc

然后，修改 conf.py ，设置其中的变量 seqdiag_fontpath 值为该路径。

* 编译

在该文档根目录，使用make命令编译成目标文件。

以编译HTML文档为例，执行：

```sh
make html
```

具体可参见 Sphinx 的使用手册
