#########################
即时消息类会话管理器
#########################

即时消息类会话管理器（imsm）管理来自不同即时消息媒体的会话。
通过对话管理、房间管理、消息转发等机制，形成客户服务系统所需要的人工对话、自动导航、消息推送、多人对话等功能。

imsm 是排队机/后台服务器的一个组成部分

***********
基本数据
***********

TODO ... ...

微信账户

微信流程路由

账户 -- 事件  -- 流程
    
****************
JSON-RPC 接口
****************

=======================
收到来自IM终端的消息
=======================

.. function:: imsm.ImMessageReceived(imtype, account, user, room_id, msgtype, msgcontent)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string
    
    :param account: 关联帐号(微信号，APPID)(微信号，APPID)
    :type account: string

    :param user: 产生该消息的IM用户的对象数据。由于消息可能由不经登录的真实账户，所以该参数需要是完整的用户对象数据，不能只是账户名。
        其格式参见 :ref:`label-user-json-format`
    :type user: object|string

    :param room_id: 该消息对应的房间ID
    :type room_id: string

    :param mstype: 消息类型。其定义参见 :doc:`msgtype`
    :type msgtype: string

    :param msgcontent: 消息内容。
        本模块不关心消息内容的具体格式，所以该参数可为任意类型。
        本模块将会把这些消息原样的转发给各个媒体接入模块，由它们进行具体内容的处理。
        对于微信，该参数一般是一个 JSON Object，将微信消息所需定义的各个属性、值记录放到对象中。

对于所有的虚拟客户端，无论哪种媒体、无论是客户还是客服，后台都不理会 ``account`` 参数。

如果是真实账户发来的消息，那么，后台要根据 ``account`` 参数更新真实账户与这个关联账户（对于微信，此关联账户就是公众号）之间的关联。

客户产生的IM消息可能不提供 ``room`` 参数，因为客户往往只有一个对话窗口。此时，后台要自行查找客户所对应的房间与会话。

=======================
收到IM终端登录成功事件
=======================

.. function:: imsm.ClientLoggedIn(imtype, account, user)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string

    :param account: 关联帐号(微信号，APPID)
    :type account: string
    
    :param user: 客户端对象数据
    :type user: object

对于所有的虚拟客户端，无论哪种媒体、无论是客户还是客服，后台都不理会 ``account`` 参数。

如果是真实账户发来的消息，那么，后台要根据 ``account`` 参数更新真实账户与这个关联账户（对于微信，此关联账户就是公众号）之间的关联。

在微信子系统中，该过程仅仅对虚拟账户有效。
当某个虚拟客户端登录验证通过后，媒体接入服务应该调用该过程。

user 参数可参见 :ref:`label-user-json-format`

.. note::
    虚拟账户的Web服务必须在某客户端的会话存在的情况下，每隔一段时间，向后台调用一次该方法。
    对于已经登录到客户端，后台认为该调用意味着续租。后台会移除长时间没有续租的客户端。

    目前，暂定90秒为续租最大超时值。也就是说，如果超过这个时间没有续租，服务器就认为客户端已经不存在。


=======================
收到IM终端注销成功事件
=======================

.. function:: imsm.ClientLoggedOut(imtype, account, user_name)

    :param imtype: IM 系统类型，微信为：weixin|app
    :type imtype: string

    :param account: 关联帐号(微信号，APPID)
    :type account: string

    :param user_name: 客户端账户ID
    :type user_name: string

对于所有的虚拟客户端，无论哪种媒体、无论是客户还是客服，后台都不理会 ``account`` 参数。

如果是真实账户发来的消息，那么，后台要根据 ``account`` 参数更新真实账户与这个关联账户（对于微信，此关联账户就是公众号）之间的关联。

在微信子系统中，该过程仅仅对虚拟账户有效。
当某个虚拟客户端注销请求到来时，媒体接入服务应该调用该过程。



========================
收到IM终端状态变化事件
========================

.. function:: imsm.ClientStatusChanged(imtype, account, user_name, status)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string

    :param account: 关联帐号(微信号，APPID)
    :type account: string

    :param user_name: 产生状态变化的IM用户账户名
    :type user_name: string

    :param status: 状态标志
    :type status: string or integer

IM客户端的状态改变、登录、注销等，均经由该RPC得到通知。

对于所有的虚拟客户端，无论哪种媒体、无论是客户还是客服，后台都不理会 ``account`` 参数。

如果是真实账户触发的调用，那么，后台要根据 ``account`` 参数更新真实账户与这个关联账户（对于微信，此关联账户就是公众号）之间的关联。

对于微信子系统，只有虚拟账户方有可能触发这个调用。

=======================
收到IM终端属性变化事件
=======================

.. function:: imsm.ClientPropsChanged(imtype, account, user_name, props)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string

    :param account: 关联帐号(微信号，APPID)
    :type account: string
    
    :param user_name: 产生状态变化的IM用户账户名
    :type user_name: string
    
    :param props: 变化后的完整属性
    :type props: object

对于所有的虚拟客户端，无论哪种媒体、无论是客户还是客服，后台都不理会 ``account`` 参数。

如果是真实账户触发的调用，那么，后台要根据 ``account`` 参数更新真实账户与这个关联账户（对于微信，此关联账户就是公众号）之间的关联。

对于微信子系统，只有虚拟账户方有可能触发这个过程。

==================
建立房间
==================

**暂不使用**

.. function:: imsm.NewRoom(imtype, account, user) -> object

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string

    :param account: 关联帐号(微信号，APPID)。对虚拟客户端无意义。
    :type account: string

    :param user: 建立房间的发起用户。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type user: string or object
    
    :return: room 对象。
    :rtype: object

为IM客户端新建一个房间。
该方法由IMA调用。

返回值是一个对象，形如：

.. code::

    {"id" : "id of the room", "expire" : 120}

它至少要有： ``id`` 属性，用于指定房间的ID； ``expire`` 属性，用于指定最大无响应时间（秒）。

如果超过最大无响应时间没有在该房间发消息或者续订，则房间将被释放。

如果发起方退出房间，且退出后房间内没有其它用户，房间也将被释放。

==================================
坐席坐在的人工对话（房间）的列表
==================================

.. function:: imsm.GetAgentRooms(imtype, user)

    :param imtype: IM 系统类型，weixin|app
    :param user: 要获取其所在人工会话的用户名。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type user: string or object

    :return: 该用户所在的人工会话的列表，列表中的元素是会话（房间）ID字符串列表
    :rtype: String[]

返回某个坐席坐在的人工对话（房间）的列表


==============
邀请进入房间
==============

.. function:: imsm.InviteEnterRoom(imtype, from_user, room_id, to_user, txt, data, expire, options)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string
    :param from_user: 发起邀请者。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type from_user: string or object
    :param room_id: 邀请进入的房间ID
    :type room_id: string
    :param to_user: 被邀请者。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type to_user: string or object
    :param txt: 邀请附加文本
    :type txt: string
    :param data: 附加的数据
    :type data: object
    :param expire: 邀请有效时间（秒）
    :type expire: float or integer
    :param options: 该用户在该房间内的设置数据
    :type options: object

===================
答复邀请进入房间
===================

.. function:: imsm.AckInviteEnterRoom(imtype, user, room_id, agreed)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string
    :param user: 当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type user: string or object
    :param room_id: 答复邀请进入的房间ID
    :type room_id: string
    :param agreed: 是否同意
    :type agreed: boolean

==========
进入房间
==========

.. function:: imsm.EnterRoom(imtype, user, room_id, options)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string
    :param user: 当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type user: string or object
    :param room_id: 房间ID
    :type room_id: string
    :param options: 该用户在该房间内的设置数据
    :type options: object

主动的将客户端加入到房间。

==========
退出房间
==========

.. function:: imsm.ExitRoom(imtype, user, room_id)

    :param imtype: IM 系统类型，weixin|app
    :type imtype: string
    :param user: 当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type user: string or object
    :param room_id: 房间ID
    :type room_id: string

主动的将客户端从房间移除。

当最后一个用户退出房间后，房间被自动释放。

