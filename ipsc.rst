#########################
IPSC 流程引擎
#########################

该程序使用 IPSC 流程引擎完成消息导航功能。

流程与后台之间采用基于 SmartBus 通信渠道的 `JSON RPC <http://www.jsonrpc.org/specification>`_

******************
IPSC 节点
******************

==========
开始节点
==========

在以下几种情况下，后台会调用流程，并由流程决定消息响应导航的过程：

#. 微信菜单点击

    此时，流程开始节点的传入参数列表为：

    .. code::

        [
            "weixin", # 消息来源的IM系统类型，微信是“weixin”
            Dialog, #消息响应导航的ID
            {
                "ToUserName": "开发者微信号",
                "FromUserName":  "发送方帐号（一个OpenID）",
                "CreateTime": "消息创建时间 （整型）",
                "MsgType": "消息类型，event",
                "Event": "事件类型，CLICK",
                "EventKey": "事件KEY值，与自定义菜单接口中KEY值对应"
            }
        ]

#. 关注

    此时，流程开始节点的传入参数列表为：

    .. code::

        [
            "weixin", # 消息来源的IM系统类型，微信是“weixin”
            Dialog, # 消息响应导航的ID
            {
                "ToUserName": "开发者微信号",
                "FromUserName": "发送方帐号（一个OpenID）",
                "CreateTime": "消息创建时间 （整型）",
                "MsgType": "消息类型，event",
                "Event": "事件类型，subscribe(订阅)、unsubscribe(取消订阅)",
            }
        ]

#. 文本消息

    .. code::

        [
            "weixin", # 消息来源的IM系统类型，微信是“weixin”
            Dialog, # 消息响应导航的ID
            {
                "ToUserName": "开发者微信号",
                "FromUserName": "发送方帐号（一个OpenID）",
                "CreateTime": "消息创建时间 （整型）",
                "MsgType": "消息类型，text",
                "Content": "文本消息内容",
                "MsgId": "消息id，64位整型",
            }
        ]

.. note::
    原本计划让后台通过Smartbus的调流程的启动信息回调函数判断调用是否成功。

    但是，考虑到异步的调用方式很难让流程在后台确定启动后在发起后续动作，所以，不再进行该判断。
    将一律认为启动成功，并开始会话，并在会话开启后5秒到15秒之间（给流程启动流程时间），循环调用 :func:`imr.exists` 判断该会话是否还存在。

==============
发送消息节点
==============

#. 参数：

    #. Dialog (in)
         消息响应导航的ID，必须与开始节点的传入参数一致

    #. Template (in)
        要发送的内容的模板名称。模板存放在数据库中。

    #. Variables (in)
        内容填充变量表，是一个 ``Dict`` 类型数据。如果没有变量，则必须是 ``None`` 。

    IPSC 将 ``Diaglog`` , ``Template`` , ``Variables`` 参数传递给后台。
    后台根据 ID 找到微信对话的双方账户，然后从数据库加载发送消息内容模板，并使用内容变量渲染到模板，最后将渲染后的内容发送给对端，然后返回这个RPC。

#. 出口
    #. 正常
    #. 错误
    #. 发送失败
    #. 调用超时

--------------
RPC
--------------

进入节点的时候，IPSC调用后台的RPC，并在返回结果后退出节点。

如果后台返回正常，从正常接口出来。

如果后台返回超时，从调用超时出口出来。

后台提供的 RPC 定义如下：

.. function:: imsm.FlowSend(dialog: string, template: str, variables: dict = {})

    :param dialog: 消息响应导航的ID，必须与开始节点的传入参数一致
    :type dialog: str
    :param template: 发送内容模板名称
    :type template: str
    :param variables: 发送内容模板填充变量表
    :type variables: dict

==============
接收消息节点
==============

#. 参数：

    #. Dialog (in)
         消息响应导航的ID，必须与开始节点的传入参数一致

    #. Timeout (in)
        等待消息接收的超时时间（单位是毫秒）

    #. Received (out)
        接收到的消息

        该输出变量是 ``dict`` 类型，它来自后台，对应于不同的消息类型，它分别包括以下属性：

        #. 文本消息

            ============= =====
            属性          描述
            ============= =====
            ToUserName    开发者微信号
            FromUserName  发送方帐号（一个OpenID）
            CreateTime    消息创建时间 （整型）
            MsgType       text
            Content       文本消息内容
            MsgId         消息id，64位整型
            ============= =====

        #. 图片消息

            ============= =====
            属性          描述
            ============= =====
            ToUserName    开发者微信号
            FromUserName  发送方帐号（一个OpenID）
            CreateTime    消息创建时间 （整型）
            MsgType       image
            PicUrl        图片链接
            MediaId       图片媒体id，可以调用多媒体文件下载接口拉取数据。
            MsgId         消息id，64位整型
            FileUrl       媒体接入服务下载到本系统文件服务器的媒体文件访问地址
            ============= =====

        #. 语音消息

            ============= =====
            属性          描述
            ============= =====
            ToUserName    开发者微信号
            FromUserName  发送方帐号（一个OpenID）
            CreateTime    消息创建时间 （整型）
            MsgType       voice
            MediaId       语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
            MsgId         消息id，64位整型
            FileUrl       媒体接入服务下载到本系统文件服务器的媒体文件访问地址
            ============= =====

        #. 视频消息
            暂不支持

        #. 地理位置消息
            暂不支持

        #. 链接消息
            暂不支持

    .. note::
        这些属性实际上是由媒体接入服务直接由微信接收消息的XML数据原样翻译的（除了FileUrl），后台并不作任何修改。

#. 出口
    #. 正常
    #. 错误
    #. 超时

--------------
RPC
--------------

进入节点的时候，IPSC调用后台的RPC，并在返回结果后退出节点。

如果后台返回正常，从正常接口出来。

如果后台返回超时，从调用超时出口出来。

后台提供的 RPC 定义如下：

.. function:: imsm.FlowRecv(dialog: string, timeout: float)->dict

    :param dialog: 消息响应导航的ID，必须与开始节点的传入参数一致
    :type dialog: str
    :param template: 发送内容模板名称
    :type template: str
    :return: 接收到的内容。对于微信，这是一个 dict 类型变量，其定义与本小节的 Received 定义相同
    :rtype: dict

==============
排队节点
==============

TODO: 完善文档...

TIPS: 参考电话部分的排队节点

==============
结束节点
==============

当消息响应导航结束，流程应该从结束节点退出，并告知后台。

.. note::
    即使流程设计器没有放置结束节点，流程结束有也必须告知后台！

    考虑用 **流程返回值** 的 SmartBus 特性，将流程结束消息带给后台！


*********************
IPSC提供的RPC接口
*********************

==============
会话验证接口
==============

**IPSC需要提供** 一个查询接口，供后台验证IM消息导航会话的存在性，用于防止流程结束通知的调用失败。

**IPSC提供** 的RPC的格式是：

.. function:: imr.exists(dialog: str) -> bool

    :param dialog: 消息响应导航的ID，必须与开始节点的传入参数一致
    :type dialog: str

    :return: 是否存在
    :rtype: bool

******************
入口流程路由规则
******************

路由表示例：

================= ========= =====
路由表达式        目标流程  说明
================= ========= =====
event.CLICK.m_1   Flow1     Key为m_1的微信菜单点击会触发流程Flow1
event.CLICK.*     Flow2     其它的所有微信菜单点击会触发流程Flow2
event.subscribe   Flow3     新的用户关注会触发流程Flow3
text              Flow4     收到普通的文本消息触发流程Flow4
================= ========= =====

**流程之上是IPSC项目，租户的项目名必须与租户的name一致！**

.. note::
    流程导航路由表记录在数据库中，由 AdminWebConsole 设置，后台程序实现，与IPSC设置无关。


******************
IMR会话超时机制
******************

==================
新对话
==================

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        A[label="IPSC"];
        B[label="BACK"];
        W[label="IM(WeiXin)"];

        W => B [label="user1: 你好", note="user1 发来消息"];

        B => B [label="流程路由", note="找到消息路由目标流程 Flow1"];

        B -> A [label="启动流程", note="新建对话，启动流程 Flow1"];
        B <--A [label="回复：启动流程成功/失败"];
    }
    
    
==================
用户消息按时接收
==================

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        A[label="IPSC"];
        B[label="BACK"];
        W[label="IM(WeiXin)"];
        
        === 向用户输出 ===
        A => B => W [label="向用户输出：你好"];
        === 开始输入等待 ===
        A -> B [label="用户输入等待开始：超时=1H", color="red"];
        === ... 30 minuts later ... ===
        W => B [label="用户输入：还在吗？"];
        A <-- B [label="用户输入等待开始结束：txt=还在吗？", color="red"];
        === 结束输入等待 ===
    }

此处，我认为，不存在消息丢失的严重风险。

如果 BACK 到 IPSC 丢失消息，只会因引发无法对客户响应的问题。当用户再次输入，可以继续！


==================
用户消息等待超时
==================

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        A[label="IPSC"];
        B[label="BACK"];
        W[label="IM(WeiXin)"];
        
        === 向用户输出 ===
        A => B => W [label="向用户输出：你好"];
        === 开始输入等待 ===
        A -> B [label="用户输入等待开始：超时=1H", color="red"];
        === ... 3600 seconds later ... ===
        B => B [label="超时", note="后台超时，不通知IPSC，以便IPSC自己从超时节点出来！"];
        A => A [label="节点超时", color="red"];
    }

如上图，后台超时，不通知IPSC，以便IPSC自己从超时节点出来。这样，两者超时的微观时间点就不重要了！

==================
流程结束会话
==================

如果流程结束，必须要通知后台结束该次IMR。如果没有通知到，后台无法得知其退出！

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        A[label="IPSC"];
        B[label="BACK"];
        
        === 等待超时，退出流程 ===

        A -> B [label="流程结束", failed, color="red"];
    }

如图，如果BACK无法收到会话结束通知，当再次有user1发送消息时，BACK将会尝试缓存该消息，并等待IPSC接收处理，造成永远无法回复user1。

---------
解决方法
---------

* 后台需要设置一个总的会话超时时间。如果一个会话，在超过一段时间后，没有得到任何流程的调用，就认为流程不存在，当解散会话。

* IPSC 利用 smartbus 流程调用的返回值回调，向后台通知流程结束。后台必须正确处理这个回调！
