####################
微信处理过程
####################

===================
非人工响应消息
===================

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        A[label="微信接入服务", color=pink];
        B[label="后台服务", color=green];

        A => B [label="ImMessageReceived"];

        B => B [label="计算消息导航文本"];

        B => A [label="SendImMessage"];
    }

=====================
关注、排队、人工服务
=====================

下图简略介绍了从关注，到进行消息导航，直到人工服务的过程

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        W[label="微信服务器", color=blue, textcolor=white]
        A[label="微信接入服务", color=pink];
        B[label="后台服务", color=green];
        C[label="WebClient服务", color=red];
        C0[label="客户1（真实终端）"];
        C1[label="客服1（虚拟终端）"];

        === 有人关注 ===

        C0 => W [label="\n关注\n"];
        W => A => B [label="\n客户1：关注\n"];

        === 推送导航消息 ===

        B => A => W [label="msg:\n给客户1：【1】人工服务\n【2】路过"];
        W => C0[label="msg:\n【1】人工服务\n【2】路过"];
        C0 => W[label="msg:\n 1\n"];
        W => A => B [label="msg:\n客户1输入： 1\n"];

        === 排队 ===
        
        B => A => W [label="msg:\n给客户1：正在排队"];
        W => C0[label="msg:\n 正在排队"];
        B => B [label="排队...\n 排到客服1"];

        === 排队成功，建立会话 ===

        B => A => C => C1 [label="通知：\n客服1与客户进行对话"];
        A => C0 [label="通知：\n客服1与客户进行对话"];

        === 会话中收到IM消息 ===
        C1 => C => A => B [label="msg:\n你好，有什么可以帮忙？"];
        B => B [label="calc:\n计算...消息转发目标=客户1"];
        B => A => W[label="msg:\n给客户1：你好，有什么可以帮忙？"];
        W => C0 [label="msg:\n你好，有什么可以帮忙？"];

        === ... ===
        
        C0 => W [label="msg:\n呃，没事了，bye!"];
        W => A => B [label="msg:\n客户1：呃，没事了，bye!"];
        B => B [label="calc:\n计算...消息转发目标=客服1"];
        B => A => C => C1 [label="msg:\n客户1：呃，没事了，bye!"]

        === 结束人工对话 ===
        C1 => C => A => B [label="命令：\n退出对话"];

        === 重新进入导航 ===
        B => A => W [label="msg:\n给客户1：你对服务满意？【Y】满意\n【N】不满意"];
        W => C0 [label="msg:\n你对服务满意？【Y】满意\n【N】不满意"];
    }


============
详细过程分解
============

下面，我们分解各个主要过程的详细步骤。


---------------------
收到微信客户端的消息
---------------------

一旦真实的微信客户端有所动作，如关注、CLICK菜单点击、发送聊天消息等，
微信服务器会将这些消息通知给媒体接入服务，
媒体接入服务进而通知后台服务器。
在非人工服务的情况下，后台服务器根据消息响应导航设置，进行消息响应。


*****************
由事件触发的导航
*****************

^^^^^^^^^^
关注事件
^^^^^^^^^^

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        W[label="微信服务器", color=blue, textcolor=white]
        A[label="微信接入服务", color=pink];
        B[label="后台服务", color=green, textcolor=yellow];
        C[label="user1（真实终端）"];

        === 关注触发了流程 ===

        C => W [label="msg:\n 关注了我们的微信客服帐号"];
        W -> A [label="HTTP:\n URL POST xml data"];
        A -> B [label="RPC:\n imsm.ImMessageReceived(user=user1, msgtype=weixin.subscribe)"];
        A <-- B [label="RPC:\n 返回"];
        W <-- A [label="HTTP:\n 200 OK"];


        === 开始执行流程 ===

        B => B [label="action:\n 加载“关注”对应的流程"];

        === 消息导航 ... ... ===
    }

^^^^^^^^^^
普通消息
^^^^^^^^^^

普通聊天消息引发的流程则有所不同。后台服务需要判断该消息是属于正在执行的流程，还是要新建流程。
这通常由超时时间控制。如果认为属于同一个流程，则继续上次的流程，否则发起新流程。

下图说明了普通消息的收、发过程。

.. seqdiag::

    seqdiag {
    
        default_fontsize = 12;

        W[label="微信服务器", color=blue, textcolor=white];
        A[label="微信接入服务", color=pink];
        B[label="后台服务", color=green, textcolor=yellow];
        C[label="user1（真实终端）"];


        === 关注之后 ... ===

        === 系统向客户端发消息 ===

        B -> A [label="RPC:\n SendImMessage(msg=【1】查余额，【2】查进度)"];
        A -> W [label="HTTP:\n POST msg"];
        A <-- W[label="HTTP:\n 200 OK"];
        B <-- A[label="RPC:\n 返回"];
        W => C[label="消息:\n 【1】查余额，【2】查进度"];

        === 客户端向系统发消息 ===

        C => W [label="消息:\n 你妹！"];
        W -> A [label="HTTP:\n URL POST msg"];
        A -> B [label="RPC:\n ImMessageReceived(msg=你妹！)"];
        A <-- B [label="RPC:\n 返回"];

        B => B [label="action:\n 后台计算"]

        === 系统向客户端发消息 ===
        
        B -> A [label="RPC:\n SendImMessage(msg=请不要讲粗话。)"];
        A -> W [label="HTTP:\n POST msg"];
        A <-- W[label="HTTP:\n 200 OK"];
        B <-- A[label="RPC:\n 返回"];
        W => C[label="消息:\n 不要讲粗话。"];

    }

^^^^^^^^^^^^^^
虚拟账户消息
^^^^^^^^^^^^^^

虚拟账户是IMA独立维护的、实际不存在的账户。用于webchat客户终端、微信客服等，需要账户、但是无法使用真实的互联网IM账户的情况。

其消息发送/接收过程参见 `关注、排队、人工服务`_ 一节。

*****************
坐席消息处理
*****************

进行人工服务时，IMSM控制多人聊天室（MUR或者ROOM），我们称为房间（ROOM）。
它通过向房间内的用户转发消息，达到对话/多人聊天的目的。
即使只有两个对话方，也需要ROOM。

本节中不再具体表现微信服务器与IMA，统一用终端到IMA的通信表示，以简化图示。

下图表现了客户要求人工服务，坐席排队，加入房间的过程:

.. seqdiag::

    seqdiag {
        default_fontsize = 12;

        W[label="微信服务", color=pink];
        B[label="后台服务", color=green, textcolor=yellow];
        G[label="guest1（真实终端）"];
        A[label="agent1（虚拟终端）"];

        G => W => B [label="msg:\n 要求人工服务"];

        B => B [label="排队:\n 排到了 agent1"];

        B -> W [label="RPC:\n guest1, agent1 加入 ROOM"];
        W => A [label="通知:\n 加入 ROOM"];
        B <-- W[label="RPC:\n 返回"];

        === ... ===
        
    }

加入房间后，如果同一个房间内的坐席或者用户发送消息，后台服务应该将消息转给房间内的其它账户。
*对于微信，真实用户不必在意房间信息，因为微信的真实客户端只会与一个系统微信帐号交互，且无法控制微信进行多窗口聊天。*

下图表现了该过程：

.. seqdiag::

    seqdiag {
        default_fontsize = 12;

        W[label="微信服务", color=pink];
        B[label="后台服务", color=green, textcolor=yellow];
        G[label="guest1（真实终端）"];
        A[label="agent1（虚拟终端）"];

        === 接上图 ===

        A => W => B [label="msg:\n 您好！热诚为您服务！"];
        B => B [label="action:\n 检查房间中的其它账户"];
        B => W => G [label="msg:\n 转发:\n 您好！热诚为您服务！"]

        === ... ===

        G => W [label="我的手机坏了，怎么维修呀？"];
        W => B [label="我的手机坏了，怎么维修呀？"];
        B => B [label="action:\n 检查房间中的其它账户"];
        B => W => A [label="msg:\n 转发:\n 我的手机坏了，怎么维修呀？"];

        === ... ===        
    }

下面是一些具体的情况

^^^^^^^^^^^^^^^^^^^^
连接坐席坐席
^^^^^^^^^^^^^^^^^^^^

假定 guest1（真实终端） 要求人工服务，且排队分配给 agent1（虚拟终端），
那么，系统应该为 guest1 与 agent1 建立房间，并邀请 agent1 加入房间，
guest1 作为真实微信账户，无法得到邀请。

.. seqdiag::

    seqdiag {
        default_fontsize = 12;

        W[label="微信服务", color=pink];
        B[label="后台服务", color=green, textcolor=yellow];
        G[label="guest1（真实终端）"];
        A[label="agent1（虚拟终端）"];

        === 为guest1选中agent1，并建立房间room1 ===

        B => B [label="新建 room1"];
        B => B [label="room1.add(guest1)"];
                
        B => W [label='RPC:\n client.AskEnterRoom(fromuser="sys", touser="agent1", room="room1")']
        W ->> A [label='询问是否接受'];
        W <<--A [label='接受'];
        W -> B [label='RPC:\n 接受进入房间邀请'];
        B => B [label='room1.add(agent1)']
        W <--B;

    }
