####################
数据字典
####################

************
SQL数据库
************


============
SQL数据表
============

---------------------
租户的IM媒体账户设置
---------------------

* 表名
    cc_mgr_t_imacc

* 字段定义

    =============== =================================== ====================
    字段            类型定义                            说明
    =============== =================================== ====================
    id              PK, varchar(50)                     账户ID
    tenant_id       FK, varchar(50)                     租户ID（广义，不用）
    unitid          FK, varchar(50)                     租户ID（狭义，用于本项目）
    acctype         varchar(50)                         账户类型，微信是 weixin
    accname         varchar(50)                         账户名，对于微信，它就是公众号
    accdata         text                                对于微信，其参数是： ``{"appid":"<微信APPID>", "appsecret":"<微信APPSCERET>"}``
    displayname     Varchar(255)                        该账户在系统中的显示名
    remark          Text                                备注信息
    enabled         Bool, default=true                  是否允许启用
    =============== =================================== ====================

------------------
租户坐席账户
------------------

* 表名
    cc_agt_t_mediaaccount

    微信坐席的帐号是一个 **虚拟帐号** ，其状态、登录等由本系统控制，**不经过腾讯微信服务器** 。
    所以我们采取类似于电话坐席和声SIP定制客户端的模式：

        在数据表中，微信坐席帐号与坐席ID关联。任何坐席，如果存在关联的微信坐席帐号，则可以使用微信坐席功能。
        在进行登录验证的时候，可以 **不考虑密码** ，直接放过。

* 字段含义

    =============== =======
    字段            说明
    =============== =======
    id              UUID
    type            必须是 ``weixin/agent``
    isenable        是否启用
    agentid         关联的坐席UUID
    =============== =======

坐席登录后，后台把相关微信账户信息传递给坐席，坐席根据信息自行向微信虚拟账户Web服务器登录。

----------------------
IMR 路由设置
----------------------
相对于IVR(Intractive Voice Response)，IMR是指 Intractive Message Response （互动消息响应），我们也可称之为消息导航。

消息导航的流程由 IPSC FlowMaker 设计，并在 IPSC 服务器上运行。

IMR 的路由表形如：

================= ========= =====
路由表达式        目标流程  说明
================= ========= =====
event.CLICK.m_1   Flow1     Key为m_1的微信菜单点击会触发流程Flow1
event.CLICK.*     Flow2     其它的所有微信菜单点击会触发流程Flow2
event.subscribe   Flow3     新的用户关注会触发流程Flow3
text              Flow4     收到普通的文本消息触发流程Flow4
text              Flow1     收到普通的WebChat文本消息触发流程Flow1
================= ========= =====

路由事件表达式可以用通配符，其事件定义有来源类型决定。

目前，可能触发IMR的微信的事件请参见 :ref:`label-微信事件消息`


该设置记录在数据库中

* 表名
    cc_mgr_imroute

* 字段定义

    =============== =================================== ====================
    字段            类型定义                            说明
    =============== =================================== ====================
    id              PK, varchar(50)                     账户ID
    name            varchar(255), not-null              路由名称
    displayname     Varchar(255)                        显示名
    accid           varchar(255), not-null, unique      账户ID， `租户的IM媒体账户设置`_ 表 ``cc_mgr_t_imacc`` 的 ID
    seq             integer, not-null                   路由序号
    expr            Varchar(255), not-null              路由规则匹配表达式
    flowid          varchar(255)                        对应的流程ID
    remark          Text                                备注信息
    enabled         Bool, default=true                  是否允许启用
    =============== =================================== ====================

----------------------
IMR 模板
----------------------

在流程设计中，发送IM数据时，不能指定静态内容，只能指定内容 *模板* 与 *模板填充变量表* ，
后台根据模板与变量表，渲染产生IM消息内容。

如果是不是动态模板，后台会直接发送，不进行渲染。

* 表名
    cc_mgr_t_imtemp

* 字段定义

    =============== =================================== ====================
    字段            类型定义                            说明
    =============== =================================== ====================
    id              PK, varchar(50)                     账户ID
    tenant_id       FK, varchar(50)                     租户ID（广义，不用）
    unitid          FK, varchar(50)                     租户ID（狭义，用于本项目）
    name            varchar(255), not-null              名称
    displayname     Varchar(255)                        显示名
    content         Text, not-null                      模板内容
    istemp          Bool, default=false                 是否动态模板
    remark          Text                                备注信息
    enabled         Bool, default=true                  是否允许启用
    =============== =================================== ====================

采用 `Jinja <http://jinja.pocoo.org/>`_ 模板格式。

----------------------
其它
----------------------

聊天记录等其它表不在此规定

*******************
IM消息
*******************

====================
IM消息格式
====================

后台在命令ima发送IM消息的时候，以及im向后台通知接收到IM消息的时候，其格式是不对称的，且需要各自进行修改，转成可识别、可被微信服务器使用的格式。

-----------------------
后台向IMA发送的IM消息
-----------------------

后台将调用 :func:`sys.SendImMessage` 发送消息， ``msgcontent`` 参数是一个 JSON Object。

IMA 需要解析该JSON字符串， **并根据 touser 参数 加上 "touser":"OPENID" 属性** 。

设置程序在图形化界面中设置模板时，应生成符合该格式的模板！

例如，为用户提供名为 hello_world 的文字消息模板，呈现给用户的模板编辑器形如：

.. code-block:: jinja

    Hello {{name}}!

填写完毕后，完整的模板应该是：

.. code-block:: jinja

    {
        "msgtype":"text",
        "text":
        {
             "content":"Hello {{name}}!"
        }
    }

这才是记录到数据库的模板设置，后台将这个模板渲染成合乎格式要求的字符串发送给IMA，最后转给微信服务器。

.. note:: 后台不关心模板与消息的具体格式，它只负责查找模板与模板渲染。


^^^^^^^^^^^^^^^
格式定义
^^^^^^^^^^^^^^^

""""""""""""""
发送文本消息
""""""""""""""

.. code::

    {
        "msgtype":"text",
        "text":
        {
             "content":"Hello World"
        }
    }

""""""""""""""
发送图片消息
""""""""""""""

.. code::

    {
        "msgtype":"image",
        "image":
        {
          "file":"/img/xxx.png" # 本地文件服务器上的文件路径，将被传到微信服务器
        }
    }

""""""""""""""
发送语音消息
""""""""""""""

.. code::

    {
        "msgtype":"voice",
        "voice":
        {
          "file":"/vms/xxx.amr" # 本地文件服务器上的文件路径，将被传到微信服务器
        }
    }

""""""""""""""
发送视频消息
""""""""""""""
暂不考虑

""""""""""""""
发送音乐消息
""""""""""""""
暂不考虑

""""""""""""""
发送图文消息
""""""""""""""

.. code::

    {
        "msgtype":"news",
        "news":{
            "articles": [
                 {
                     "title":"Happy Day",
                     "description":"Is Really A Happy Day",
                     "url":"URL",
                     "picurl":"PIC_URL"
                 },
                 {
                     "title":"Happy Day",
                     "description":"Is Really A Happy Day",
                     "url":"URL",
                     "picurl":"PIC_URL"
                 }
             ]
        }
    }

----------------------------
IMA向后台通知接收到的IM消息
----------------------------

后台提供 RPC :func:`imsm.ImMessageReceived` 用于接收IM消息。

后台并不关系消息的具体格式，但是，后台将消息转给IPSC的FLOW计算交互式消息响应过程时，FLOW对消息格式是有要求的。
所以，IMA必须提供合乎规范的消息格式！

^^^^^^^^^^^^^^^
格式定义
^^^^^^^^^^^^^^^

对于微信， :func:`imsm.ImMessageReceived` 的 ``msgtype`` 与 ``msgcontent`` 参数规定如下：

"""""""""""""""""""
关注/取消关注事件
"""""""""""""""""""

* ``msgtype``

    * 关注： ``event.subscribe``
    * 取消关注： ``event.unsubscribe``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 123456789, # 消息创建时间 （整型）,
            "MsgType" : "event", #消息类型
            "Event" : "subscribe | unsubscribe", #事件类型， subscribe (订阅)、 unsubscribe (取消订阅)
        }

"""""""""""""""""""
上报地理位置事件
"""""""""""""""""""

* ``msgtype`` : ``event.LOCATION``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 58093284, # 消息创建时间 （整型）,
            "MsgType" : "event", #消息类型
            "Event" : "LOCATION", #事件类型
            "Latitude" : 23.137466, #地理位置纬度
            "Longitude" : 113.352425, #地理位置经度
            "Precision" : 119.385040,  #地理位置精度
        }

"""""""""""""""""""
自定义菜单事件
"""""""""""""""""""

* ``msgtype`` : ``event.CLICK.<EVENTKEY>``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 58093284, # 消息创建时间 （整型）,
            "MsgType" : "event", #消息类型
            "Event" : "CLICK", #事件类型
            "EventKey" : "C1", #事件KEY值，与自定义菜单接口中KEY值对应
        }

""""""""""
文本消息
""""""""""

* ``msgtype`` : ``text``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 58093284, # 消息创建时间 （整型）,
            "MsgType" : "text", #消息类型
            "Content" : "this is a test", #文本消息内容
            "MsgId" : "1234567890123456", #消息id，64位整型
        }

""""""""""
图片消息
""""""""""

* ``msgtype`` : ``image``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 58093284, # 消息创建时间 （整型）,
            "MsgType" : "image", #消息类型
            "PicUrl" : "this is a url", #图片链接
            "MediaId" : "media_id", #图片消息媒体id，可以调用多媒体文件下载接口拉取数据。
            "MsgId" : "1234567890123456", #消息id，64位整型
            "File" : "/xxx/xxx/xxx.png", #下载到本地的文件服务器后的文件路径
        }

""""""""""
语音消息
""""""""""

* ``msgtype`` : ``voice``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 58093284, # 消息创建时间 （整型）,
            "MsgType" : "voice", #消息类型
            "PicUrl" : "this is a url", #图片链接
            "MediaId" : "media_id", #语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
            "MsgId" : "1234567890123456", #消息id，64位整型
            "File" : "/xxx/xxx/xxx.amr", #下载到本地的文件服务器后的文件路径
        }

"""""""""
视频消息
"""""""""
暂不考虑

"""""""""""""
地理位置消息
"""""""""""""

* ``msgtype`` : ``location``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 58093284, # 消息创建时间 （整型）,
            "MsgType" : "location", #消息类型
            "Location_X" : 23.134521, #地理位置维度
            "Location_Y" : 113.358803, #地理位置经度
            "Scale" : 20, #地图缩放大小
            "Label" : "地理位置信息",
            "MsgId" : "1234567890123456", #消息id，64位整型
        }


""""""""""
链接消息
""""""""""

* ``msgtype`` : ``link``

* ``msgcontent``

    .. code::

        {
            "ToUserName" : "开发者微信号",
            "FromUserName" : "发送方帐号（一个OpenID）",
            "CreateTime" : 58093284, # 消息创建时间 （整型）,
            "MsgType" : "link", #消息类型
            "Title" : "标题",
            "Description" : "描述",
            "Url" : "http://xxx.com/xxx.html", # 链接地址
            "MsgId" : "1234567890123456", #消息id，64位整型
        }

-----------------------
区别两种IM消息
-----------------------

按照上文的规定，来自用户的IM消息，与系统主动生成的，要向用户发送的IM消息格式是完全不同的。

在有人工客户服务人员参与的情况下，系统可能需要向人工客服、真实账户来回转发消息。
也就是说，IMA 的 RPC :func:`sys.SendImMessage` 收到的消息，格式可能是系统默认产生的消息（格式对应于微信的发送客服消息），也有可能是用户/虚拟用户产生的消息（格式对应于微信的接收消息）。

此时，IMA 需要翻译这些异构消息！

如果消息的JSON OBJECT中有属性 ``ToUserName`` ``FromUserName`` ，可认为该消息是被转发的，来自客户端的消息，向真实账户发送这些消息时，需要翻译成微信任何的格式。
如果是向虚拟客户端发送，可由客户端自行翻译。

====================
用户类型定义
====================

===========  =====
usertype 值  含义
===========  =====
0            未指定
1            不指定客服/客户的实际客户端账户
2            客户实际客户端账户
3            客服实际客户端账户
4            不指定客服/客户的虚拟客户端账户
5            客户虚拟客户端账户
6            客服虚拟客户端账户
===========  =====

====================
微信虚拟客户端状态
====================

===========  =====
status 值    含义
===========  =====
-1           未登录
0            已登录
1            空闲
2            离开
3            忙碌
===========  =====


