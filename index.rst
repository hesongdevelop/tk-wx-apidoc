.. weixin subsystem api document documentation master file, created by
   sphinx-quickstart on Mon Feb 17 10:06:03 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

微信子系统 API 手册
=========================================================

内容目录
--------

.. toctree::
   :maxdepth: 2

   即时消息类媒体接入服务 <ima>
   即时消息类会话管理器 <imsm>
   即时消息虚拟客户端Web服务 <webclient>
   IPSC 消息响应流程 <ipsc>
   微信处理过程 <weixinseq>
   消息类型定义 <msgtype>
   数据字典 <datadict>
   媒体文件存储 <filestore>

索引与搜索
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

