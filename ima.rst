#########################
即时消息类媒体接入服务
#########################

在本体系内，微信接入程序是一个即时消息类媒体接入服务（IMA）。
它使用与其它IMA相同的API定义。

IMA 通过 ``JSON-RPC over SmartBus`` 与会话管理程序以及坐席IM终端Web服务器交互。

本章节定义了这些 JSON-RPC 的接口以及工作过程。

**************
基本概念
**************

=================
IM客服账号
=================

媒体接入系统需要同时管理多个客服帐号。

对于微信，客服帐号是指微信公众号。此时，IMA需要同时处理多个来自微信服务器的，由不同微信公众号产生的消息。

IMA通过读数据库的设置，获知这些帐号的名称、ID、密码等信息。

一个客服帐号对应多个客户账户与坐席账户

=====================
实际账户与虚拟账户
=====================

通常，系统将客服帐号的联系人账户，或者向客服帐号发送消息的账户视作客户账户。这些都是实际账户。

某些特殊情况下，客户账户是没有登录到集体的媒体系统的，如用于WebChat时，客户并不会登录到微信或者QQ。
此时，客户账户是虚拟的，他们由IMA管理，而不是IM提供者。

坐席账户一般是虚拟的账户，其状态由IMA控制，并由IMA控制其消息收发。

坐席也可是实际账户，如使用某个实际的QQ账户，并将其作为客服人员号码。

IMA 不必关心账户是客服还是客户，它只需要区分虚拟与实际账户，且保证实际账户之间、实际账户与虚拟账户之间、以及虚拟账户之间的消息通畅与格式统一。

imsm 不必关心账户是实际账户还是虚拟账户，它只需要区分坐席账户与客户账户，并管理房间与会话。

=====================
房间
=====================

当两个或者账户账户交谈的时候，它们必须处于同一个房间内。多窗口聊天的时候，可由房间ID区别消息所属的聊天窗口。

IMA只需要记录房间ID与聊天窗口的关系，房间中不同帐号的消息转发、可见性由imsm实现。

来自客户账户的消息可以不指定房间，发给客户账户的消息也可不指定房间——客户通常在且只在一个房间中。

==========================
数据格式
==========================

.. _label-user-json-format:

--------
客户端
--------

:func:`GetClientInfo` 返回的客户端数据格式定义如下：

.. code-block:: javascript

    {
        type: "weixin", // [ **必须** ] 帐号类型
        user: "50298423", // [ **必须** ] 用户帐号。对于微信这是客户，这是一个 OPENID
        usertype: 2, // [ **必须** ] 用户帐号类型
        username: "西门扫雪",
        status: 302, // 状态，IMA根据具体情况自行定义
        props: {
            avartar : "xxx.png",
            geolocation : [1321.123, 234.22],
            "..." : "..."
        }, // 附加属性，不同的媒体，其属性可大为不同，由IMA根据具体情况自行定义
        data: {}, // 附加数据
    }

用户类型定义：

===========  =====
usertype 值  含义
===========  =====
0            未指定
1            不指定客服/客户的实际客户端账户
2            客户实际客户端账户
3            客服实际客户端账户
4            不指定客服/客户的虚拟客户端账户
5            客户虚拟客户端账户
6            客服虚拟客户端账户
===========  =====

*******************
IMA 的任务触发
*******************
==============
微信普通消息
==============

todo...

================
虚拟客户端消息
================

todo...

**************
JSON-RPC 接口
**************

=======================
Backend RPC 接口
=======================

这些接口由后台程序调用

----------------
发消息给客户端
----------------

.. function:: SendImMessage(imtype, account, fromuser, touser, room, msgcontent)

    :param imtype: 消息类型。目前 ``weixin`` 表示微信消息， ``app`` 表示对app发送
    :type imtype: string

    :param account: 通过该帐号发送消息。当向客服发送数据时，该字段必须为 ``None`` ；当向客户端发送的时候，表示目标客户端账户（微信账户/APPID）
    :type account: string

    :param fromuser: 产生该消息的IM用户账户名
    :type fromuser: string

    :param touser: 该消息接收目标的IM用户账户名
    :type touser: string

    :param room: 房间ID
    :type room: string

    :param msg: 消息内容。

该过程由后台调用。后台通过调用该过程向客户端发送或者转发消息。

收到该请求之后，应该以 ``fromuser`` 的名义，向 ``touser`` 发送该消息，
并在确定消息发送成功后，进行函数返回。如果消息发送失败，则返回错误信息。
当 ``touser`` 是微信实际账户时， ``fromuser`` 无意义，因为微信的真实客户端只会显示对接的公众号，亦即 ``account`` 为发送者。

对于微信，如果目标账户是实际账户，则需要通过微信服务器，使用 ``account`` 对应的公众号码向目标发送消息。
由于向虚拟客户端发送消息无需经过公众号码或者代理账户， ``account`` 参数对于虚拟账户之间的消息发送无意义。
此时，该参数可为 ``null`` 。

多聊天窗口条件下，客户端可根据 room 值的不同，决定在哪个窗口显示消息。
向客户发送的消息，可不指定 ``fromuser`` ，因为很多情况下，客户并不需要知道发送消息的具体人员。
同理，由于客户通常只有一个聊天窗口， ``room`` 参数可以不必指定（为 ``null`` ）。

本模块不关心消息内容的具体格式，所以该参数可为任意类型。
本模块将会把这些消息原样的转发给各个媒体接入模块，由它们进行具体内容的处理。
对于微信，该参数一般是一个 JSON Object，将微信消息所需定义的各个属性、值记录放到对象中。

----------------
返回终端状态
----------------

.. function:: GetClientStatus(user)

    :param account: IM媒体帐号ID。用于在多账户时的区别。
    :type account: string

    :return: 账户的状态

某些系统的账户是没有状态的，如微信。有些情况下，可能无法获取实际账户的状态，例如账户不在联系人列表中。

即使对于微信这样无状态的系统，其对应的坐席虚拟帐号也应该提供状态管理机制！

----------------
返回终端总数
----------------

.. function:: GetClientCount(account, status_filter=None)

    :param account: IM媒体帐号ID。用于在多账户时的区别。
    :type account: string

    :param status_filter: 状态过滤条件。统计数量时，只返回满足状态过滤条件的终端。默认为 NULL ，表示无过滤条件，统计所有。
    :type status_filter: string

    :return: 统计得到的数量
    :rtype: integer

----------------
返回终端信息
----------------

.. function:: GetClient(account, user)

    :param account: IM媒体帐号ID。用于在多账户时的区别。
    :type account: string

    :param user: 要获取数据的用户的账户ID或ID列表。
        当传入字符串的时候，应获取该帐号的信息；
        当传入字符串数组时，应以数组形式返回该数组中帐号的信息。
    :type user: string or array of string

    :return: 账户信息或者账户信息列表
    :rtype: object or array of object

--------------------------------
按范围返回获得终端信息
--------------------------------

.. function:: RangeGetClient(account, start, end)

    :param account: IM媒体帐号ID。用于在多账户时的区别。
    :type account: string

    :param start: 开始序号（0开始，开区间）
    :type start: integer

    :param end: 结束序号（0开始，闭区间）
    :type end: integer

    :return: 账户信息列表
    :rtype: array of object

--------------------------------
告知收到加入房间的邀请
--------------------------------

.. function:: Invited(im_type, account, from_user, room_id, to_user, txt, data, expire, options)

    :param imtype: IM的类型， weixin|app
    :param account: 账户，用于表示该邀请的来源账户（微信号/APPID）
    :param from_user: 发起邀请者。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type from_user: string or object
    :param room_id: 邀请进入的房间ID
    :type room_id: string
    :param to_user: 被邀请者。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type to_user: string or object
    :param txt: 邀请附加文本
    :type txt: string
    :param data: 附加的数据
    :type data: object
    :param expire: 邀请有效时间（秒）
    :type expire: float or integer
    :param options: 该用户在该房间内的设置数据
    :type options: object

该通知来自后台。收到后，应向被邀请的客户端推送“被邀请”通知。

--------------------
告知房间被关闭
--------------------

.. function:: RoomDisposed(to_account, to_user, room_id, txt)
    
    :param to_account: 接受该消息通知的IM客户端所对应的账户。如果客户端是虚拟的，则该参数无效
    :type to_account: str

    :param to_user: 接受该消息通知的IM客户端
    :type to_user: str or dict

    :param room_id: 加入/退出的房间(会话)ID
    :type room_id: str    

**当房间（会话中）关闭的时候，后台应该通知每一个会话内的客户端！**

--------------------------------
告知已经进入房间
--------------------------------

.. function:: EnteredRoom(to_account, to_user, cause_account, cause_user, room_id, txt)

    :param to_account: 接受该消息通知的IM客户端所对应的账户。如果客户端是虚拟的，则该参数无效
    :type to_account: str

    :param to_user: 接受该消息通知的IM客户端
    :type to_user: str or dict

    :param cause_account: 导致该消息产生的IM客户端所对应的账户。如果客户端是虚拟的，则该参数无效
    :type cause_account: str

    :param cause_user: 导致该消息产生的IM客户端所对应的账户
    :type to_user: str or dict

    :param room_id: 加入/退出的房间(会话)ID
    :type room_id: str

该过程应该由IMSM调用，用于告知IM账户进入了房间。

收到该调用后，应该告知IM Web Client修改聊天窗口。

**当房间（会话中）有加入/退出事件发生的时候，后台应该通知每一个会话内的客户端！**

--------------------------------
告知已经离开房间
--------------------------------

.. function:: ExitedRoom(to_account, to_user, cause_account, cause_user, room_id, txt)

    :param to_account: 接受该消息通知的IM客户端所对应的账户。如果客户端是虚拟的，则该参数无效
    :type to_account: str

    :param to_user: 接受该消息通知的IM客户端
    :type to_user: str or dict

    :param cause_account: 导致该消息产生的IM客户端所对应的账户。如果客户端是虚拟的，则该参数无效
    :type cause_account: str

    :param cause_user: 导致该消息产生的IM客户端所对应的账户
    :type to_user: str or dict

    :param room_id: 加入/退出的房间(会话)ID
    :type room_id: str

该过程应该由IMSM调用，用于告知IM账户进入了房间。

收到该调用后，应该告知IM Web Client修改聊天窗口。

**当房间（会话中）有加入/退出事件发生的时候，后台应该通知每一个会话内的客户端！**

****************
WebSocket 客户端
****************

==================
Client->Server 
==================

WebSocket的地址是： ``ws://<host>[:port]/weChatAdapter/websocket/wechatMessage?working_num=<座席工号>``

客户端到服务器的 WebSocket 通信采用 JSON-RPC 形式。

客户端发出请求，服务器应按照请求执行程序，并返回执行结果。

客户端不得同时发起多个请求。

-----
登录
-----

视连接作登录

-----
注销
-----

视断开连接作登录

----------------
发消息
----------------


"""""
例子
"""""

1. 发送消息

    客户端发送文本：

    .. code-block:: json

        {
            "id": "0001",
            "action": "sendMessage",
            "params": {
                "working_num": "工号",
                "room_id": "房间ID",
                "msgtype": "text|image",
                "content": "亲[亲]，你好哦！| image_url"
            }
        }

    服务器回复：

    .. code-block:: json

        {
            "id": "0001",
            "errcode": 0,
            "errmsg" : "ok" 
        }

        OR

        {
            "id": "0001",
            "errcode": 12345,
            "errmsg" : "error message" 
        }

收到该调用后，应该调用后台的 :func:`imsm.ImMessageReceived` 通知后台，
并在该 RPC 返回之后，方可返回结果。后台将根据会话状态、房间状态决定如果处理这条由客户端发送的消息。

----------------
答复加入房间请求
----------------

.. function:: ack_invite(room_id:str, agreed: bool, txt:str="")

    :param room_id: 房间的ID
    :param agreed: 是否同意
    :param txt: 附加文本

当IM终端答复后，使用该方法通知IMA。
此时，IMA应调用后台的 :func:`imsm.AckInviteEnterRoom` 过程进行告知，
并在后台返回后，方才可以返回。

如果客户端同意，在其收到正确返回后，可认为加入房间成功。

IMA 可自行选择将同意、拒绝消息通知给房间内的各方。

"""""
例子
"""""
客户端同意进入房间 ``5298``：

.. code-block:: json

    {
        "id": "0003",
        "action": "ack_invite",
        "params": {
            "working_num": "工号",
            "room_id": "5298",
            "agreed": true
        }
    }

服务器回复：

.. code-block:: json

        {
            "id": "0001",
            "errcode": 0,
            "errmsg" : "ok" 
        }


==================
Server->Client 
==================

服务器到客户端的WebSocket通信是单向的通知消息，采用JSON-RPC Notification格式

----------------------
告知收到了IM消息
----------------------

.. function:: message_received(user, room_id, msgtype, msgbody)

    :param user: 发起消息者。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type user: string or object
    :param room_id: 房间ID
    :type room_id: string
    :param msgtype: 消息类型
    :type msgtype: string
    :param msgbody: 消息体

收到该通知后，客户端应在聊天信息窗口显示该消息。

"""""
例子
"""""

客户端收到的消息格式::

    {
        "msgtype": "text|image|voice",
        "sender": "",
        "roomId": "房间ID",
        "content": "文本或者URL"
    }

----------------------
告知收到加入房间的邀请
----------------------

.. function:: invited(user, room_id, txt, data, expire, options)

    :param user: 发起邀请者。当该参数为字符串，表示用户名；当该参数为对象，表示该用户的对象数据。
    :type user: string or object
    :param room_id: 邀请进入的房间ID
    :type room_id: string
    :param txt: 邀请附加文本
    :type txt: string
    :param data: 附加的数据
    :type data: object
    :param expire: 邀请有效时间（秒）
    :type expire: float or integer
    :param options: 该用户在该房间内的设置数据
    :type options: object

收到该通知后，客户端可调用 :func:`agent.ack_invite` 进行答复

"""""
例子
"""""

客户端收到邀请的消息格式::

    {
        "msgtype": "invitation",
        "toUser": "客户端工号",
        "roomId": "房间ID",
        "expire": "超时时间（毫秒）"
    }

客户端收到用户进入房间的消息格式：

    {
        "msgtype": "enteredRoom",
        "sender": "...",
        "toUser": "客户端工号",
        "roomId": "房间ID"
    }

客户端收到用户离开房间的消息格式：

    {
        "msgtype": "exitedRoom",
        "sender": "...",
        "toUser": "客户端工号",
        "roomId": "房间ID"
    }

客户端收到房间被释放的消息格式：

    {
        "msgtype": "roomDisposed",
        "toUser": "客户端工号",
        "roomId": "房间ID"
    }    


**************
HTTP 客户端
**************

.. warning:: HTTP定义过期，请使用 `WebSocket 客户端`_

对于微信子系统，坐席Web客户端是依托于该服务的，使用虚拟账户的客户端。

其登录、注销、对话均通过WebAPI调用，借由本服务完成。

特性：

* RESTful 风格的 HTTP Web API

* 提供完备的坐席HTML界面

* 完全兼容 Internet Explorer 8.0

* 采用 VLC Player 的 HTML 嵌入插件为坐席播放微信语音

* 同时提供用于 WebChat 的外网多租户虚拟账户，包括可定制的 HTML 界面，以及跨域访问管理。

* 完全兼容 Chrome 30+, Firefox 27+, Internet Explorer 11+ 等现代浏览器

==================
Client->Server 
==================

这些WebAPI用于坐席Web客户端调用，大多数API都需要IMA整理收到的HTTP请求，然后转发给后台，并根据后台返回的结果进行HTTP回复。

在URL路径中，“<>”之间部分表示必须变量，“[]”之间部分表示可选变量。

----------------
登录
----------------

* HTTP REQUEST

    * URL
        /client/<账户>/login

    * METHOD
        POST

    * HEADERS
        * Content-Type
            application/json

    * CONTENT
        JSON 数据，形如：

        .. code::

            {"password" : "your_password"}

对于微信，该方法仅仅可作用于虚拟账户。

该过程应该由客户端调用，用于虚拟客户端账户的登录。

登录时，如果密码验证通过，就应该调用后台的 :func:`imsm.ClientLoggedIn` ，通知后台。
如果后台的这个过程返回失败，即使密码验证通过，也应认为登录失败，并向客户端返回失败信息。

----------------
注销
----------------

* HTTP REQUEST

    * URL
        /client/<账户>/logout

    * METHOD
        POST

对于微信，该方法仅仅可作用于虚拟账户。

该过程应该由客户端调用，用于虚拟客户端账户的注销。

注销时，应该调用后台的 :func:`imsm.ClientLoggedOut` ，通知后台。
如果后台的这个过程返回失败，则认为注销失败，并向客户端返回失败信息。

----------------
发消息
----------------

* HTTP REQUEST

    * URL
        /client/<账户>/msg

    * METHOD
        POST

    * HEADERS

        * Content-Type
            application/json

    * CONTENT
        JSON 数据，不同类型的消息，其格式有所不同

        * 文本消息

            .. code-block:: javascript

                {
                    msgtype : "text",
                    room_id : "<房间ID>",
                    text : {
                        content : "Hello World", //文本消息内容
                    }
                }

        * 图片消息

            .. code-block:: javascript

                {
                    msgtype : "image",
                    room_id : "<房间ID>",
                    image : {
                        file_url: "<文件服务器上的媒体文件地址>",
                    }
                }

        * 语音消息
            不支持

        * 视频消息
            不支持

        * 音乐消息
            不支持

        * 图文消息
            不支持

该过程用于实现 **虚拟客户端** 的消息发送。
当虚拟客户端要发送消息的时候，调用该过程。

收到该调用后，应该调用后台的 :func:`imsm.ImMessageReceived` 通知后台，
并在该 RPC 返回之后，方可返回结果。后台将根据会话状态、房间状态决定如果处理这条由客户端发送的消息。

----------------
获取/设置状态
----------------

* HTTP REQUEST

    * URL
        /client/<账户>/status

    * METHOD
        * GET  用于获取
        * POST 用于设置

    * HEADERS
        * Content-Type
            application/json

    * CONTENT
        设置状态时，形如：
        
        .. code-block:: javascript

            {status: "<状态值>"}

* HTTP RESPONSE
    仅在GET时返回状态对象

    * HEADERS
        * Content-Type
            application/json

    * CONTENT

        .. code-block:: javascript

            {status: "<状态值>"}
    
IMA 收到该调用后，应修改对应终端的状态，并在确定修改成功后返回。
如果修改失败，则需要返回错误信息。

.. note:: 某些系统的账户是没有状态的，如微信。同时，实际账户的状态是无法由IMA修改的。

.. note:: 即使对于微信这样无状态的系统，其对应的虚拟帐号也应该提供状态管理机制！

----------------
获取/设置属性
----------------

* HTTP REQUEST

    * URL
        /client/<账户>/props

    * METHOD
       * GET用于获取
       * POST用于设置

    * HEADERS
        * Content-Type
            application/json

    * CONTENT
        JSON 数据，形如：

        .. code-block:: javascript

            {
                props : {
                    key1 : "value1",
                    key2 : "value2",
                } // 要设置的属性。它是一个 JSON Object，IMA应将新设置的属性合并到原有属性。
            }

* HTTP RESPONSE
    仅在GET时返回状态对象

    * HEADERS
        * Content-Type
            application/json

    * CONTENT
        形如：

        .. code-block:: javascript

            {
                props : {
                    key1 : "value1",
                    key2 : "value2"
            }
            
--------------------------------
新建/查询/续订房间
--------------------------------

* HTTP REQUEST

    * URL
        /room/[room_id]

    * METHOD
       * GET  用于获取/续订
       * POST 用于新建

       GET需要指定room_id

    * ARGUMENT
        GET的时候，设置参数 ``renew=1`` 表示获取房间信息的同时进行续订。

* HTTP RESPONSE

    * HEADERS
        * Content-Type
            application/json

    * CONTENT

        返回房间的信息，形如：

        .. code-block:: javascript

            {
                "id" : "<room_id>",
                "expire" : 120,
                "created_by" : "<user_id>",
                "created_at" : "YYYY-MM-DDTHH:mm:ss.zzz",
                "users" : [
                    {"id": "user1", "..." : "..."},
                    {"id": "user2", "..." : "..."},
                ]
            }

客户端调用该过程，建立一个新的房间。房间是由后台控制的，所以得调用后台的新建房间RPC，并返回结果。

客服端得在 ``expire`` 属性指定的时间（秒）到来之前续订房间，否则后台会将客户端逐出房间。

如果是新建房间，应调用后台的 :func:`imsm.NewRoom` ，并在其返回后方可HTTP回复。

如果是获取房间信息，应调用后台的 :func:`imsm.GetRoom` ，并在其返回后方可HTTP回复。

--------------------------------
邀请/回复加入房间
--------------------------------

邀请某个坐席加入对话。发起邀请方必须事先位于一个房间中，且要邀请中指定要该房间。

* HTTP REQUEST

    * URL
        /client/<账户>/invite

    * METHOD
       * POST表示发起邀请
       * PUT表示回复邀请
       
    * CONTENT

        发起邀请时，内容格式是：
        
        .. code-block:: javascript

            {
                "user": "<user1>", // 被邀请者
                "room_id" : "<room id>", // 邀请加入的房间的ID
                "options" : {}, // 该用户在该房间内的设置数据
                "expire" : 120, // 邀请有效时限（秒）
                "txt" : "[邀请文本]", // 邀请附加文本
            }

        答复邀请时，内容格式是：

        .. code-block:: javascript

            {
                "room_id" : "<room id>", // 邀请加入的房间的ID
                "agreed" : true, // 是否同意
                "txt" : "[附加文本]", // 邀请答复附加文本
            }

ima收到邀请调用后，应该调用后台的RPC :func:`imsm.InviteEnterRoom` ，通知后台；
然后，ima自行通知被邀请的IM终端，并在客户端显示请求询问界面。

当IM终端答复后，使用该方法通知IMA。
此时，IMA应调用后台的 :func:`imsm.AckInviteEnterRoom` 过程进行告知，
并在后台返回后，方才可以进行HTTP返回。

如果客户端同意，在其收到 HTTP 200 后，可认为加入房间成功。

IMA 可自行选择将同意、拒绝消息通知给房间内的各方。

--------------------------------
直接进入进入房间
--------------------------------

坐席直接尝试进入指定的房间。

* HTTP REQUEST

    * URL
        /client/<账户>/enter_room

    * METHOD
       POST
       
    * CONTENT
        
        .. code-block:: javascript

            {
                "room_id" : "<room id>", // 房间的ID
                "options" : {}, // 该用户在该房间内的设置数据
            }

IMA收到该请求后，调用后台的 :func:`imsm.EnterRoom` 。

--------------------------------
直接进入退出房间
--------------------------------

坐席直接尝试退出指定的房间。

* HTTP REQUEST

    * URL
        /client/<账户>/exit_room

    * METHOD
       POST
       
    * CONTENT
        
        .. code-block:: javascript

            {
                "room_id" : "<room id>", // 房间的ID
            }

IMA收到该请求后，调用后台的 :func:`imsm.ExitRoom` 。

==================
Server->Client
==================

建议参照 W3C 的 `Server-Sent Events <http://www.w3.org/TR/eventsource/>`_ 标准（简称为 SSE）。

按照该标准，实现基于长轮询机制的SSE。

服务器为每个客户端提供一个单一的SSE访问点，所有类型的事件通知均从该URL获得。

IMA需要自行实现SSE所需的客户端JS代码、可靠事件、用户验证、消息缓存等特性。

* URL

    /client/<帐号>/sse

* 事件数据格式

    所有的事件都应该是经过转义，不包含特殊字符的JSON OBJECT字符串。
    其中不允许有 ``\r`` ， ``\n`` 字符，以适应SSE标准。

--------------------------------
事件定义
--------------------------------

^^^^^^^^^^^^
收到IM消息
^^^^^^^^^^^^

数据格式形如：

    .. code-block:: javascript

        {
            type: "msg",
            params: {
                fromuser: "user1",
                touser: "user2",
                room_id: "room1",
                msgtype: "text|image|...",
                "...": {...} // 根据具体的消息格式而异的数据
        }

客户端收到该事件后，应设法显示该IM消息。

^^^^^^^^^^^
收到邀请
^^^^^^^^^^^

如果邀收到了加入房间的邀请，IMA应该向被邀请者推送此数据。

数据格式形如：

    .. code-block:: javascript

        {
            type: "invited",
            params: {
                user: {...},
                room_id: "<id>",
                txt: "...",
                data: {...}
                expire: 30,
                option: {...}
            }
        }

客户端收到该事件后，应设法更新显示结果。

^^^^^^^^^^^^^
收到邀请答复
^^^^^^^^^^^^^

如果邀请被同意或者拒绝，IMA应该向发起邀请者推送此数据。

数据格式形如：

    .. code-block:: javascript

        {
            type: "invite-ack",
            params: {
                from_user: {...},
                room_id: "<id>",
                agreed: true | false,
            }
        }


客户端收到该事件后，应设法更新显示结果。

^^^^^^^^^^^
进入了房间
^^^^^^^^^^^

数据格式形如：

    .. code-block:: javascript

        {
            type: "entered_room",
            params: {
                user: {...},
                room_id: "<id>",
            }
        }

客户端收到该事件后，应设法更新显示结果。
        
^^^^^^^^^^^^^^
离开了房间
^^^^^^^^^^^^^^

数据格式形如：

    .. code-block:: javascript

        {
            type: "existed_room",
            params: {
                user: {...},
                room_id: "<id>",
            }
        }

客户端收到该事件后，应设法更新显示结果。

